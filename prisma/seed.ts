/* eslint-disable no-console */
import { Prisma } from '@prisma/client';
import { hashSync } from 'bcrypt';
import { prisma } from '../src/utils/prismaClient';

async function createPermissions() {
  console.log('starting permissions creation ...');
  const permissions: Prisma.PermissionCreateInput[] = [
    {
      name: 'Postman',
    },
    {
      name: 'Admin',
    },
    {
      name: 'Manager',
    },
  ];

  for (const { name } of permissions) {
    await prisma.permission.upsert({
      update: {},
      where: { name },
      create: { name },
    });
    console.log('permission ', name, ' inserted');
  }

  console.log('Permissions created!\n--------------------------');
}

async function createUnities() {
  console.log('starting unities creation...');

  const unities: Prisma.UnitiesCreateInput[] = [
    {
      id: 'd5b91098-a30f-45eb-b4d3-e70dc683ccf1',
      address: {
        connectOrCreate: {
          create: {
            id: 'aa19d844-8075-4573-8253-8ef9df6736d0',
            address: 'Rua Miguel Montoro Lozano',
            city: 'Florianópolis',
            country: 'Brazil',
            neighborhood: 'Nova Brasília de Valéria',
            number: 49,
            state: 'SC',
          },
          where: {
            id: 'aa19d844-8075-4573-8253-8ef9df6736d0',
          },
        },
      },
      name: 'Unidade Floripa',
    },
    {
      id: '8e3c0afe-29fe-4f1c-beea-b991369a2d5e',
      address: {
        connectOrCreate: {
          create: {
            id: '5a78366d-6505-4f39-a5f4-52388f468f7b',
            address: 'Rua Otílio Dalçoquio',
            city: 'Itajaí',
            country: 'Brazil',
            neighborhood: 'Salseiros',
            number: 753,
            state: 'SC',
          },
          where: {
            id: '5a78366d-6505-4f39-a5f4-52388f468f7b',
          },
        },
      },
      name: 'Unidade de Itajaí',
    },
    {
      id: '475cd197-6b8b-4335-b5f9-b14a33b34d65',
      address: {
        connectOrCreate: {
          create: {
            id: '888567b4-56db-4d0f-8ee7-4598876cbdad',
            address: 'Rua José Bressan',
            city: 'Tubarão',
            country: 'Brazil',
            neighborhood: 'Monte Castelo',
            number: 55,
            state: 'SC',
          },
          where: {
            id: '888567b4-56db-4d0f-8ee7-4598876cbdad',
          },
        },
      },
      name: 'Unidade de Tubarão',
    },
    {
      id: '92cbb409-5a27-443e-b94d-dcd70c011f530',
      address: {
        connectOrCreate: {
          create: {
            id: 'cfc8d95c-7079-4eb4-a108-02b800e87936',
            address: 'Rua Pascoal Meler',
            city: 'Criciúma',
            country: 'Brazil',
            neighborhood: 'Universitário',
            number: 250,
            state: 'SC',
          },
          where: {
            id: 'cfc8d95c-7079-4eb4-a108-02b800e87936',
          },
        },
      },
      name: 'Unidade de Criciúma',
    },
  ];

  for (const data of unities) {
    await prisma.unities.upsert({
      create: data,
      update: {},
      where: { id: data.id },
    });
    console.log('unity ', data.name, ' created!');
  }

  console.log('Unities created!');
}

async function createLogTypes() {
  console.log('starting log types creation...');

  const logTypes: Prisma.LogTypeCreateInput[] = [
    {
      name: 'Entrada no sistema',
      id: '0faf2e5d-fb59-4cae-89f1-b592154b3b63',
    },
    {
      name: 'Entrada na unidade',
      id: 'b1ffde92-a41b-49c5-9de8-1cb898625ade',
    },
    {
      name: 'Saída da unidade',
      id: '8672dc63-c5d6-4a6a-b308-613e8beb841b',
    },
    {
      name: 'Saída para entrega',
      id: '054145b2-7dd5-4803-b9f8-77ac41a5cbcf',
    },
    {
      name: 'Pacote entregue',
      id: '793bb30f-03e2-42f0-a6a4-0e8735f78e8e',
    },
  ];

  for (const { name, id } of logTypes) {
    await prisma.logType.upsert({
      create: { name, id },
      update: { id },
      where: { name },
    });

    console.log('log type: ', name, ' created!');
  }

  console.log('Log Types created!');
}

async function createPostManAndManager() {
  const users = [
    {
      email: 'Manager@man.com',
      name: 'Gerente Massa',
      passwordHash: hashSync('gerente123', 12),
      UserPermissions: {
        connectOrCreate: {
          create: {
            id: 'de52d0f4-5939-4d0e-8820-9441f65c980b',
            Permission: {
              connect: {
                name: 'Manager',
              },
            },
          },
          where: {
            id: 'de52d0f4-5939-4d0e-8820-9441f65c980b',
          },
        },
      },
    },
    {
      email: 'Carteiro@carta.com',
      name: 'Carteiro Massa',
      passwordHash: hashSync('carteiro123', 12),
      UserPermissions: {
        connectOrCreate: {
          create: {
            id: 'de52d0f4-5939-4d0e-8820-9441f65c880b',
            Permission: {
              connect: {
                name: 'Postman',
              },
            },
          },
          where: {
            id: 'de52d0f4-5939-4d0e-8820-9441f65c880b',
          },
        },
      },
    },
  ];

  for (const user of users) {
    await prisma.user.upsert({
      create: user,
      update: {},
      where: { email: user.email },
    });
  }
}

async function main() {
  console.log('seed is running ...');
  await createPermissions();
  await createUnities();
  await createLogTypes();
  await createPostManAndManager();
}

main()
  .catch((e) => {
    console.error(e);
    process.exit(1);
  })
  .finally(async () => {
    await prisma.$disconnect();
  });
