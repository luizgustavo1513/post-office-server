import { sign } from 'jsonwebtoken';
import 'dotenv/config';

// TYPES

export class GenericToken {
  generateToken({ tokenData, time }: { tokenData: object; time: string }) {
    const secret: any = process.env.JWT_SECRET;

    return sign(tokenData, secret, { expiresIn: time });
  }
}
