const frontendURL =
  process.env.NODE_ENV === 'production' ? '' : 'exp://10.109.0.15:19000/--/';

export { frontendURL };
