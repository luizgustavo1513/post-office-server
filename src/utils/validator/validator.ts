// TYPES
import { IValidator } from './types';

// SERVICES
import { ServerMessage } from '../messages/serverMessage';

export class Validator {
  notNull(Vars: IValidator[]) {
    for (const { variable, label } of Vars) {
      if (!variable) {
        throw new ServerMessage({
          statusCode: 400,
          message: `Verifique a informação: ${label} e tente novamente.`,
        });
      }
    }
  }

  cannotExists(Vars: IValidator[]) {
    for (const { label, variable } of Vars) {
      if (variable) {
        throw new ServerMessage({
          statusCode: 400,
          message: `A informação: ${label} já existe na base de dados.`,
        });
      }
    }
  }

  needExist(Vars: IValidator[]) {
    for (const { label, variable } of Vars) {
      if (!variable) {
        throw new ServerMessage({
          statusCode: 404,
          message: `A informação: ${label} não existe na base de dados.`,
        });
      }
    }
  }

  typeof(Vars: { label: string; variable: any; type: 'string' | 'number' }[]) {
    for (const { label, type, variable } of Vars) {
      /* eslint-disable-next-line */
      if (typeof variable !== type) {
        throw new ServerMessage({
          statusCode: 404,
          message: `A informação: ${label} deve ser do tipo ${type}.`,
        });
      }
    }
  }
}
