import 'dotenv/config';
import * as nodemailer from 'nodemailer';

export class Transporter {
  public mailer;

  constructor() {
    this.mailer = nodemailer.createTransport({
      host: 'smtp.mail.us-west-2.awsapps.com',
      port: 465,
      secure: true,
      auth: {
        user: process.env.EMAIL_USERNAME,
        pass: process.env.EMAIL_PASSWORD,
      },
      tls: { rejectUnauthorized: false },
    });
  }
}
