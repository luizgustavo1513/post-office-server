// TYPES
import { Response, Request, NextFunction } from 'express';

// CLASS
import { PermissionServices } from '../../api/client/permission/services/permissionServices';

const permissionServices = new PermissionServices();

export const isManager = async (
  req: Request,
  _res: Response,
  next: NextFunction,
) => {
  const permissions = req.Permissions;

  await permissionServices.checkPermission({
    userPermissions: permissions,
    permission: 'Manager',
  });

  next();
};
