import { ServerMessage } from '../../../../utils/messages/serverMessage';
import { prisma } from '../../../../utils/prismaClient';

export class AccessTokensServices {
  async create({ token, userId }: { token: string; userId?: string }) {
    await prisma.accessToken.create({
      data: {
        token,
        hasUsed: false,
        userId,
      },
    });
  }

  async findByToken({ token }: { token: string }) {
    return prisma.accessToken.findUnique({
      where: {
        token,
      },
    });
  }

  async disable({ token }: { token: string }) {
    await prisma.accessToken.update({
      data: {
        hasUsed: true,
      },
      where: {
        token,
      },
    });
  }

  async verifyActive({ token }: { token: string }) {
    const existingToken = await prisma.accessToken.findFirst({
      where: {
        token,
        hasUsed: true,
      },
    });

    if (existingToken) {
      throw new ServerMessage({ message: 'Token inválido!', statusCode: 400 });
    }
  }
}
