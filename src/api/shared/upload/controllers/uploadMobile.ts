/* eslint-disable */

import { Request, Response } from 'express';
import multer from 'multer';
import aws from 'aws-sdk';
import path from 'path';
import s3Storage from 'multer-sharp-s3';
import { ServerMessage } from '../../../../utils/messages/serverMessage';

export async function uploadMobile(req: Request, res: Response) {
  const s3bucket = new aws.S3({
    accessKeyId: process.env.AWS_ACCESS_KEY_ID,
    secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
    region: 'us-west-2',
  });

  const mobileUpload = multer({
    storage: s3Storage({
      s3: s3bucket,
      Bucket: process.env.AWS_S3_BUCKET,
      ACL: 'public-read',
      withMetadata: true,
      Key: function (_req: any, file: any, cb: any) {
        cb(
          null,
          path.basename(file.originalname, path.extname(file.originalname)) +
            '-' +
            Date.now() +
            path.extname(file.originalname),
        );
      },
    }),
  }).single('file');

  mobileUpload(req, res, (error) => {
    if (error) {
      throw new ServerMessage({
        statusCode: 400,
        message: 'Erro ao efetuar upload do arquivo.',
      });
    } else if (req.file === undefined) {
      throw new ServerMessage({
        statusCode: 400,
        message: 'Erro ao efetuar upload do arquivo.',
      });
    } else {
      let URL = req.file as any;
      return res.status(200).json({ url: URL.Location });
    }
  });
}
