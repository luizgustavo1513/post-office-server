// LIBS
import { Router } from 'express';

// FUNCTIONS
import { upload } from './controllers/upload';
import { uploadMobile } from './controllers/uploadMobile';

// ROUTES
export const uploadRouter = Router();

uploadRouter.post('/file', upload);

uploadRouter.post('/file/mobile', uploadMobile);
