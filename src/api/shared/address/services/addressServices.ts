import { prisma } from '../../../../utils/prismaClient';

export class AddressServices {
  async findById({ addressId }: { addressId: string }) {
    const address = await prisma.address.findUnique({
      where: {
        id: addressId,
      },
    });

    return address;
  }

  async createAddress({
    address,
    city,
    country,
    neighborhood,
    number,
    state,
  }: {
    address: string;
    city: string;
    country: string;
    neighborhood: string;
    number: number;
    state: string;
  }) {
    const newAddress = await prisma.address.create({
      data: {
        address,
        city,
        country,
        neighborhood,
        number,
        state,
      },
    });

    return newAddress;
  }
}
