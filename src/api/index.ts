import { Router } from 'express';
import { clientRouter } from './client/client.routes';

// ROUTES
export const routes: Router = Router();

routes.use('/client', clientRouter);

// BACKOFFICE
