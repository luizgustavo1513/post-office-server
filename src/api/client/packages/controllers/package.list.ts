import { Request, Response } from 'express';
import { ServerMessage } from '../../../../utils/messages/serverMessage';
import { Validator } from '../../../../utils/validator/validator';
import { UserServices } from '../../users/user/services/userServices';
import { UserPermissionServices } from '../../users/userPermission/services/userPermissionServices';
import { PackageServices } from '../services/packageServices';

const validator = new Validator();
const userServices = new UserServices();
const userPermissionServices = new UserPermissionServices();
const packageServices = new PackageServices();

export async function listPackageController(req: Request, res: Response) {
  const { userId } = req;
  const { isDelivered } = req.query;

  validator.notNull([{ label: 'usuário', variable: userId }]);

  const user = await userServices.findById({ userId });

  validator.needExist([{ label: 'usuário', variable: user }]);

  const permissions = await userPermissionServices.findPermissionsByUser({
    userId,
  });

  validator.needExist([
    { label: 'permissões de usuário', variable: permissions[0] },
  ]);

  let isManager = false;
  let isPostman = false;
  for (const { Permission } of permissions) {
    if (Permission.name === 'Manager') {
      isManager = true;
    }
    if (Permission.name === 'Postman') {
      isPostman = true;
    }
  }

  if (isManager === true) {
    const packages = await packageServices.findAllPackages();
    return res.status(200).json(packages);
  }

  if (isPostman === true) {
    if (isDelivered) {
      const packages = await packageServices.findDeliveredPackages();
      return res.status(200).json(packages);
    } else {
      const packages = await packageServices.findAllPostmanPackages();
      return res.status(200).json(packages);
    }
  }

  throw new ServerMessage({
    message: 'Usuário não possui permissões para visualizar dispositivos',
    statusCode: 400,
  });
}
