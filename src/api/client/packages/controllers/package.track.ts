import { Request, Response } from 'express';
import { arrivedId } from '../../../../utils/logIds';
import { ServerMessage } from '../../../../utils/messages/serverMessage';
import { Validator } from '../../../../utils/validator/validator';
import { UnityServices } from '../../unities/services/unityServices';
import { PackageLogServices } from '../services/packageLogServices';
import { PackageServices } from '../services/packageServices';

const validator = new Validator();
const pkgServices = new PackageServices();
const pkgLogServices = new PackageLogServices();
const unityServices = new UnityServices();

export async function trackPackageController(req: Request, res: Response) {
  const { rfid, unityId } = req.body;

  validator.notNull([
    { label: 'rfid', variable: rfid },
    { label: 'unidade', variable: unityId },
  ]);

  const pkg = await pkgServices.findByRfid({ rfid: rfid as string });

  validator.needExist([{ label: 'pacote', variable: pkg }]);

  const unity = await unityServices.findById({ unityId: unityId as string });

  validator.needExist([{ label: 'unidade', variable: unity }]);

  const nextUnityLog = await pkgLogServices.findNextPkgLog({
    packageId: pkg!.id,
  });

  validator.needExist([{ label: 'Rota cadastrada', variable: nextUnityLog }]);

  if (nextUnityLog!.unitiesId !== unityId) {
    throw new ServerMessage({
      message: 'Pacote em unidade incorreta',
      statusCode: 400,
    });
  }

  if (nextUnityLog!.logTypeId === arrivedId) {
    throw new ServerMessage({
      message: 'Esta ação apenas pode ser realizada pelo entregador',
      statusCode: 400,
    });
  }

  await pkgLogServices.updateHereLog({ pkgLogId: nextUnityLog!.id });

  return res.status(200).json({
    ServerMessage: {
      statusCode: 200,
      message: 'Caminho de pacote atualizado com sucesso',
    },
  });
}
