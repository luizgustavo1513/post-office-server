import { Request, Response } from 'express';
import { arrivedId } from '../../../../utils/logIds';
import { ServerMessage } from '../../../../utils/messages/serverMessage';
import { Validator } from '../../../../utils/validator/validator';
import { PackageLogServices } from '../services/packageLogServices';
import { PackageServices } from '../services/packageServices';

const validator = new Validator();
const pkgLogServices = new PackageLogServices();
const pkgServices = new PackageServices();

export async function deliverPackageController(req: Request, res: Response) {
  const { packageId } = req.params;
  const { imageUrl } = req.body;

  validator.notNull([{ label: 'pacote', variable: packageId }]);
  validator.notNull([{ label: 'image', variable: imageUrl }]);

  const pkg = await pkgServices.findById({
    packageId,
  });

  validator.needExist([{ label: 'pacote', variable: pkg }]);

  const log = await pkgLogServices.findNextPkgLog({ packageId });

  if (log?.logTypeId !== arrivedId) {
    throw new ServerMessage({
      message: 'Ação ainda não pode ser realizada',
      statusCode: 400,
    });
  }

  await pkgLogServices.updateHereLog({
    pkgLogId: log!.id,
  });

  await pkgServices.addDeliveredPhoto({
    imageUrl,
    packageId,
  });

  return res.status(200).json({
    ServerMessage: {
      message: 'Pacote entregue com sucesso',
      status: 200,
    },
  });
}
