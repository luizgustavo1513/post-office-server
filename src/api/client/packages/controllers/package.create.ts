import { Request, Response } from 'express';
import { Validator } from '../../../../utils/validator/validator';
import { UnityServices } from '../../unities/services/unityServices';
import { PackageLogServices } from '../services/packageLogServices';
import { PackageServices } from '../services/packageServices';

const validator = new Validator();
const packageServices = new PackageServices();
const packageLogServices = new PackageLogServices();
const unityServices = new UnityServices();

export async function createPackageController(req: Request, res: Response) {
  const { rfid, unityId } = req.body;

  validator.notNull([
    { label: 'rfid', variable: rfid },
    { label: 'unidade', variable: unityId },
  ]);

  const pkg = await packageServices.findByRfid({ rfid: rfid as string });

  validator.cannotExists([{ label: 'rfid', variable: pkg }]);

  const unity = await unityServices.findById({ unityId: unityId as string });

  validator.needExist([{ label: 'Unidade', variable: unity }]);

  const pkgCreated = await packageServices.create({ rfid: rfid as string });

  await packageLogServices.createInitialLog({
    packageId: pkgCreated.id,
    unityId: unityId as string,
  });

  return res.status(200).json({
    ServerMessage: {
      StatusCode: 200,
      message: 'Pacote registrado com sucesso',
    },
  });
}
