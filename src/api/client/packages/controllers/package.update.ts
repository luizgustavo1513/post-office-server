import { Request, Response } from 'express';
import {
  arrivedId,
  checkoutLogId,
  entryLogId,
  exitLogId,
} from '../../../../utils/logIds';
import { ServerMessage } from '../../../../utils/messages/serverMessage';
import { Validator } from '../../../../utils/validator/validator';
import { AddressServices } from '../../../shared/address/services/addressServices';
import { PackageLogServices } from '../services/packageLogServices';
import { PackageServices } from '../services/packageServices';

const validator = new Validator();
const pkgServices = new PackageServices();
const pkgLogServices = new PackageLogServices();
const addressServices = new AddressServices();

export async function updatePackageController(req: Request, res: Response) {
  const {
    routesArray,
    weight,
    invoice,
    orderId,
    address,
    city,
    country,
    neighborhood,
    number,
    state,
    name,
    packageId,
  } = req.body;
  const { userId } = req;

  validator.notNull([
    { label: 'peso', variable: weight },
    { label: 'id de pedido', variable: orderId },
    { label: 'nota fiscal', variable: invoice },
    { label: 'endereço', variable: address },
    { label: 'cidade', variable: city },
    { label: 'país', variable: country },
    { label: 'bairro', variable: neighborhood },
    { label: 'número', variable: number },
    { label: 'estado', variable: state },
    { label: 'nome', variable: name },
    { label: 'rfid', variable: packageId },
  ]);

  validator.typeof([
    { label: 'peso', variable: weight, type: 'number' },
    { label: 'id de pedido', variable: orderId, type: 'string' },
    { label: 'nota fiscal', variable: invoice, type: 'string' },
    { label: 'endereço', variable: address, type: 'string' },
    { label: 'cidade', variable: city, type: 'string' },
    { label: 'país', variable: country, type: 'string' },
    { label: 'bairro', variable: neighborhood, type: 'string' },
    { label: 'número', variable: number, type: 'number' },
    { label: 'estado', variable: state, type: 'string' },
    { label: 'nome', variable: name, type: 'string' },
    { label: 'rfid', variable: packageId, type: 'string' },
  ]);

  const newAddress = await addressServices.createAddress({
    address,
    city,
    country,
    neighborhood,
    number,
    state,
  });

  const createdAddress = await addressServices.findById({
    addressId: newAddress.id,
  });

  validator.needExist([{ label: 'endereço', variable: createdAddress }]);

  const pkg = await pkgServices.findByIdToEdit({ packageId });

  validator.needExist([{ label: 'pacote', variable: pkg }]);

  if (pkg!.PackageLogs.length > 1) {
    throw new ServerMessage({
      message: 'Rota já foi definida para este pacote',
      statusCode: 400,
    });
  }

  if (!Array.isArray(routesArray)) {
    throw new ServerMessage({
      message: 'Tipo inválido enviado para rotas',
      statusCode: 400,
    });
  }

  const nextDate = new Date(pkg!.PackageLogs[0].estimateArriveDate!);
  nextDate.setDate(nextDate.getDate() + 1);

  await pkgLogServices.createLogs({
    estimateArriveDate: nextDate,
    logTypeId: exitLogId,
    packageId,
    unitiesId: pkg?.PackageLogs[0].unity.id!,
  });

  for (const data of routesArray) {
    validator.needExist([
      { label: 'identificador de unidade', variable: data.unitiesId },
      { label: 'data de chegada estimada', variable: data.estimateArriveDate },
    ]);

    validator.typeof([
      {
        label: 'data de chegada estimada',
        type: 'string',
        variable: data.estimateArriveDate,
      },
    ]);

    data.estimateArriveDate = new Date(data.estimateArriveDate);
    data.packageId = packageId;

    await pkgLogServices.createLogs({
      estimateArriveDate: data.estimateArriveDate,
      logTypeId: entryLogId,
      packageId,
      unitiesId: data.unitiesId,
    });

    if (data.unitiesId !== routesArray[routesArray.length - 1].unitiesId) {
      let nextDate = new Date(data.estimateArriveDate!);
      nextDate.setDate(nextDate.getDate() + 1);

      await pkgLogServices.createLogs({
        estimateArriveDate: nextDate,
        logTypeId: exitLogId,
        packageId,
        unitiesId: data.unitiesId,
      });
    }
  }

  await pkgLogServices.createLogs({
    estimateArriveDate: routesArray[routesArray.length - 1].estimateArriveDate,
    logTypeId: checkoutLogId,
    packageId,
    unitiesId: routesArray[routesArray.length - 1].unitiesId,
  });

  await pkgLogServices.createLogs({
    estimateArriveDate: routesArray[routesArray.length - 1].estimateArriveDate,
    logTypeId: arrivedId,
    packageId,
    unitiesId: routesArray[routesArray.length - 1].unitiesId,
  });

  await pkgServices.updatePKG({
    addressId: newAddress.id,
    invoice,
    name,
    orderId,
    packageId,
    userId,
    weight,
  });

  return res.status(200).json({
    ServerMessage: {
      statusCode: 200,
      message: 'Pacote editado com sucesso',
    },
  });
}
