import { Request, Response } from 'express';
import { Validator } from '../../../../utils/validator/validator';
import { PackageServices } from '../services/packageServices';

const validator = new Validator();
const pkgServices = new PackageServices();

export async function findPackageInfoController(req: Request, res: Response) {
  const { packageId } = req.params;

  validator.notNull([{ label: 'pacote', variable: packageId }]);

  const pkg = await pkgServices.findById({ packageId });

  validator.needExist([{ label: 'pacote', variable: pkg }]);

  return res.status(200).json(pkg);
}
