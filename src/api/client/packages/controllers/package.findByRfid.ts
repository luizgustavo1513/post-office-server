import { Request, Response } from 'express';
import { Validator } from '../../../../utils/validator/validator';
import { PackageServices } from '../services/packageServices';

const validator = new Validator();
const pkgServices = new PackageServices();

export async function findPackageByRfid(req: Request, res: Response) {
  const { rfid } = req.query;

  validator.notNull([
    {
      label: 'rfid',
      variable: rfid,
    },
  ]);

  const pkg = await pkgServices.findByRfid({ rfid: rfid as string });

  return res.status(200).json({ hasPackage: pkg ? true : false });
}
