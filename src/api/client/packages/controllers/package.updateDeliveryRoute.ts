import { Request, Response } from 'express';
import { checkoutLogId } from '../../../../utils/logIds';
import { ServerMessage } from '../../../../utils/messages/serverMessage';
import { Validator } from '../../../../utils/validator/validator';
import { PackageLogServices } from '../services/packageLogServices';
import { PackageServices } from '../services/packageServices';

const validator = new Validator();
const packageServices = new PackageServices();
const pkgLogServices = new PackageLogServices();

export async function updateDeliveryRoutePackage(req: Request, res: Response) {
  const { packageId } = req.params;

  validator.notNull([{ label: 'pacote', variable: packageId }]);

  const pkg = await packageServices.findById({ packageId });

  validator.needExist([{ label: 'pacote', variable: pkg }]);

  const nextUnityLog = await pkgLogServices.findNextPkgLog({
    packageId: pkg!.id,
  });

  validator.needExist([{ label: 'Registro de rota', variable: nextUnityLog }]);

  if (nextUnityLog!.logTypeId !== checkoutLogId) {
    throw new ServerMessage({
      message: 'Esta ação ainda não pode ser realizada',
      statusCode: 400,
    });
  }

  await pkgLogServices.updateHereLog({
    pkgLogId: nextUnityLog!.id,
  });

  await packageServices.updateInRoute({
    packageId,
    status: pkg!.inDeliveryRoute,
  });

  return res.status(200).json({
    ServerMessage: {
      message: 'Rota de pacote atualizada com sucesso',
      statusCode: 200,
    },
  });
}
