import { Router } from 'express';
import { authMiddleware } from '../../../middlewares/auth';
import { isManager } from '../../../middlewares/permissions/isManager';
import { createPackageController } from './controllers/package.create';
import { deliverPackageController } from './controllers/package.deliver';
import { findPackageByRfid } from './controllers/package.findByRfid';
import { findPackageInfoController } from './controllers/package.info';
import { listPackageController } from './controllers/package.list';
import { trackPackageController } from './controllers/package.track';
import { updatePackageController } from './controllers/package.update';
import { updateDeliveryRoutePackage } from './controllers/package.updateDeliveryRoute';

export const packageRouter = Router();

packageRouter.post('/create', createPackageController);

packageRouter.put(
  '/update',
  authMiddleware,
  isManager,
  updatePackageController,
);

packageRouter.put('/track', trackPackageController);

packageRouter.get('/info/:packageId', findPackageInfoController);

packageRouter.get('/list', authMiddleware, listPackageController);

packageRouter.put(
  '/update/route/:packageId',
  authMiddleware,
  updateDeliveryRoutePackage,
);

packageRouter.get('/find', findPackageByRfid);

packageRouter.put(
  '/deliver/:packageId',
  authMiddleware,
  deliverPackageController,
);
