import { prisma } from '../../../../utils/prismaClient';

export class PackageLogServices {
  async createInitialLog({
    packageId,
    unityId,
  }: {
    packageId: string;
    unityId: string;
  }) {
    await prisma.packageLogs.create({
      data: {
        arriveDate: new Date(),
        estimateArriveDate: new Date(),
        wasHere: true,
        logTypes: {
          connect: {
            name: 'Entrada no sistema',
          },
        },
        package: {
          connect: {
            id: packageId,
          },
        },
        unity: {
          connect: {
            id: unityId,
          },
        },
      },
    });
  }

  async createLogs(data: {
    logTypeId: string;
    packageId: string;
    unitiesId: string;
    estimateArriveDate?: Date;
  }) {
    await prisma.packageLogs.create({
      data,
    });
  }

  async findNextPkgLog({ packageId }: { packageId: string }) {
    const pkgLog = await prisma.packageLogs.findFirst({
      where: {
        packageId,
        wasHere: false,
      },
      orderBy: {
        createdAt: 'asc',
      },
    });

    return pkgLog;
  }

  async updateHereLog({ pkgLogId }: { pkgLogId: string }) {
    await prisma.packageLogs.update({
      data: {
        wasHere: true,
        arriveDate: new Date(),
      },
      where: {
        id: pkgLogId,
      },
    });
  }
}
