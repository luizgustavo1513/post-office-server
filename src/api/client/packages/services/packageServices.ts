import { arrivedId } from '../../../../utils/logIds';
import { prisma } from '../../../../utils/prismaClient';

export class PackageServices {
  async create({ rfid }: { rfid: string }) {
    const pkg = await prisma.package.create({
      data: {
        rfid,
      },
    });

    return pkg;
  }

  async findById({ packageId }: { packageId: string }) {
    const pkg = await prisma.package.findFirst({
      select: {
        address: {
          select: {
            address: true,
            city: true,
            country: true,
            neighborhood: true,
            number: true,
            state: true,
            id: true,
          },
        },
        id: true,
        invoice: true,
        name: true,
        orderId: true,
        rfid: true,
        inDeliveryRoute: true,
        user: {
          select: {
            id: true,
            email: true,
            name: true,
            profilePicture: true,
          },
        },
        weight: true,
        PackageLogs: {
          select: {
            arriveDate: true,
            estimateArriveDate: true,
            id: true,
            logTypes: {
              select: {
                id: true,
                name: true,
              },
            },
            unity: {
              select: {
                id: true,
                name: true,
                address: {
                  select: {
                    address: true,
                    city: true,
                    country: true,
                    neighborhood: true,
                    number: true,
                    state: true,
                    id: true,
                  },
                },
              },
            },
            wasHere: true,
          },
          orderBy: {
            estimateArriveDate: 'asc',
          },
        },
      },
      where: {
        id: packageId,
        name: {
          not: null,
        },
      },
    });
    return pkg;
  }

  async findByIdToEdit({ packageId }: { packageId: string }) {
    const pkg = await prisma.package.findUnique({
      include: {
        PackageLogs: {
          include: {
            unity: true,
          },
        },
        address: true,
      },
      where: {
        id: packageId,
      },
    });
    return pkg;
  }

  async findByRfid({ rfid }: { rfid: string }) {
    const pkg = await prisma.package.findUnique({
      where: {
        rfid,
      },
    });

    return pkg;
  }

  async updatePKG({
    invoice,
    name,
    orderId,
    packageId,
    addressId,
    userId,
    weight,
  }: {
    packageId: string;
    invoice: string;
    name: string;
    addressId: string;
    orderId: string;
    weight: number;
    userId: string;
  }) {
    await prisma.package.update({
      data: {
        invoice,
        name,
        addressId,
        orderId,
        weight,
        userId,
      },
      where: { id: packageId },
    });
  }

  async findAllPostmanPackages() {
    const ids: [] = await prisma.$queryRawUnsafe(`
    select "packageId" as id  from "packageLogs" pl 
    where
    exists(select 1
    from "packageLogs" pl2 
    where 
    pl2."packageId" = pl."packageId" and
    pl2."logTypeId" = '0faf2e5d-fb59-4cae-89f1-b592154b3b63' and "wasHere" = true)
    and ((select count(*) 
    from "packageLogs" pl2 
    where 
    pl2."packageId" = pl."packageId" and
    pl2."logTypeId" = 'b1ffde92-a41b-49c5-9de8-1cb898625ade' and "wasHere" = true)
    =
    (select count(*) 
    from "packageLogs" pl2 
    where 
    pl2."packageId" = pl."packageId" and
    pl2."logTypeId" = 'b1ffde92-a41b-49c5-9de8-1cb898625ade'))
    and ((select count(*)
    from "packageLogs" pl2 
    where 
    pl2."packageId" = pl."packageId" and
    pl2."logTypeId" = '8672dc63-c5d6-4a6a-b308-613e8beb841b' and "wasHere" = true)
    =
    (select count(*) 
    from "packageLogs" pl2 
    where 
    pl2."packageId" = pl."packageId" and
    pl2."logTypeId" = '8672dc63-c5d6-4a6a-b308-613e8beb841b'))
    and exists(select 1  
    from "packageLogs" pl2 
    where
    pl2."packageId" = pl."packageId" and
    pl2."logTypeId" = '793bb30f-03e2-42f0-a6a4-0e8735f78e8e' and "wasHere" = false)
    and exists(select 1 
    from "packageLogs" pl2 
    where 
    pl2."packageId" = pl."packageId" and
    pl2."logTypeId" = '054145b2-7dd5-4803-b9f8-77ac41a5cbcf' and "wasHere" = true)
    
    group by "packageId"
`);

    const packages = await prisma.package.findMany({
      select: {
        id: true,
        rfid: true,
        name: true,
        weight: true,
        invoice: true,
        orderId: true,
        inDeliveryRoute: true,
        address: {
          select: {
            address: true,
            city: true,
            country: true,
            neighborhood: true,
            number: true,
            state: true,
          },
        },
      },
      where: {
        OR: ids,
      },
    });

    return packages;
  }

  async findAllPackages() {
    const packages = await prisma.package.findMany({
      select: {
        id: true,
        rfid: true,
        name: true,
        weight: true,
        invoice: true,
        orderId: true,
        inDeliveryRoute: true,
      },
      where: {
        addressId: {
          equals: null,
        },
      },
    });

    return packages;
  }

  async findDeliveredPackages() {
    const packages = await prisma.package.findMany({
      select: {
        id: true,
        rfid: true,
        name: true,
        weight: true,
        invoice: true,
        orderId: true,
        inDeliveryRoute: true,
        deliveredPhotoUrl: true,
        PackageLogs: {
          select: {
            arriveDate: true,
          },
          where: {
            logTypeId: arrivedId,
          },
        },
      },
      where: {
        deliveredPhotoUrl: {
          not: null,
        },
      },
    });

    return packages;
  }

  async updateInRoute({
    packageId,
    status,
    imageUrl,
  }: {
    packageId: string;
    status: boolean;
    imageUrl?: string;
  }) {
    await prisma.package.update({
      data: {
        inDeliveryRoute: !status,
        deliveredPhotoUrl: imageUrl,
      },
      where: {
        id: packageId,
      },
    });
  }

  async addDeliveredPhoto({
    imageUrl,
    packageId,
  }: {
    imageUrl: string;
    packageId: string;
  }) {
    await prisma.package.update({
      data: { deliveredPhotoUrl: imageUrl },
      where: {
        id: packageId,
      },
    });
  }
}
