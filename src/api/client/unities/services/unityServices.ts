import { prisma } from '../../../../utils/prismaClient';

export class UnityServices {
  async findById({ unityId }: { unityId: string }) {
    const unity = await prisma.unities.findUnique({
      where: {
        id: unityId,
      },
    });

    return unity;
  }
}
