import { prisma } from '../../../../../utils/prismaClient';
import { ICreateUserPermission } from '../types';

export class UserPermissionServices {
  async createUserPermission({ userId, permissionId }: ICreateUserPermission) {
    await prisma.userPermissions.create({
      data: {
        userId,
        permissionId,
      },
    });
  }

  async findByName({ permName }: { permName: string }) {
    const perm = await prisma.permission.findUnique({
      where: {
        name: permName,
      },
    });

    return perm;
  }

  async findPermissionsByUser({ userId }: { userId: string }) {
    const permissions = await prisma.userPermissions.findMany({
      select: {
        Permission: {
          select: {
            name: true,
          },
        },
      },
      where: {
        userId,
      },
    });

    return permissions;
  }
}
