import { Router } from 'express';
import { authMiddleware } from '../../../middlewares/auth';
import { changePasswordController } from './user/controllers/user.changePassword';
import { updateProfilePicture } from './user/controllers/user.changeProfilePicture';
import { createUserController } from './user/controllers/user.create';
import { userInfoController } from './user/controllers/user.info';
import { userLoginController } from './user/controllers/user.login';
import { updateUserController } from './user/controllers/user.update';

export const userRouter = Router();

userRouter.post('/create', createUserController);

userRouter.post('/login', userLoginController);

userRouter.get('/info/:userId', authMiddleware, userInfoController);

userRouter.put('/update/:userId', authMiddleware, updateUserController);

userRouter.patch(
  '/update/password/:userId',
  authMiddleware,

  changePasswordController,
);

userRouter.patch(
  '/update/profilepicture/:userId',
  authMiddleware,
  updateProfilePicture,
);
