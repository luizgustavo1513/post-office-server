import { Request, Response } from 'express';
import { Validator } from '../../../../../utils/validator/validator';
import { UserServices } from '../services/userServices';

const userServices = new UserServices();
const validator = new Validator();

export async function userInfoController(req: Request, res: Response) {
  const { userId } = req.params;

  validator.notNull([{ label: 'id de usuário', variable: userId }]);

  const user = await userServices.findById({ userId });

  validator.needExist([{ label: 'usuário', variable: user }]);

  return res.status(200).json({
    id: user?.id,
    name: user?.name,
    email: user?.email,
    profilePicture: user?.profilePicture,
  });
}
