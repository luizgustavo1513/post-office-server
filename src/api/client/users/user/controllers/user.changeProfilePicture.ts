import { Request, Response } from 'express';
import { Validator } from '../../../../../utils/validator/validator';
import { UserServices } from '../services/userServices';

const validator = new Validator();
const userServices = new UserServices();

export async function updateProfilePicture(req: Request, res: Response) {
  const { userId } = req.params;
  const { profilePicture } = req.body;

  validator.notNull([
    { label: 'foto de perfil', variable: profilePicture },
    { label: 'id de usuário', variable: userId },
  ]);

  validator.typeof([
    { label: 'foto de perfil', type: 'string', variable: profilePicture },
    { label: 'id de usuário', type: 'string', variable: userId },
  ]);

  const user = await userServices.findById({ userId });

  validator.needExist([{ label: 'usuário', variable: user }]);

  await userServices.updateProfilePicture({ profilePicture, userId });

  return res.sendStatus(200);
}
