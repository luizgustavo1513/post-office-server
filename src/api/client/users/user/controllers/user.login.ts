import { Request, Response } from 'express';
import { Validator } from '../../../../../utils/validator/validator';
import { HandlerToken } from '../../../../../utils/token/handlerToken';
import { AuthServices } from '../../../auth/services/authServices';
import { UserServices } from '../services/userServices';

const validator = new Validator();
const auth = new AuthServices();
const userServices = new UserServices();
const tokenHandler = new HandlerToken();

export async function userLoginController(req: Request, res: Response) {
  const { email, password } = req.body;

  validator.notNull([
    { label: 'email', variable: email },
    { label: 'senha', variable: password },
  ]);

  validator.typeof([
    { label: 'email', type: 'string', variable: email },
    { label: 'senha', type: 'string', variable: password },
  ]);

  const user = await auth.findByEmail({ email });

  validator.needExist([{ label: 'usuário', variable: user }]);

  await auth.canLogin({ password, user });

  await userServices.updateLastAccess({ userId: user.id });

  const token = tokenHandler.generateToken({
    tokenData: { userId: user.id, Permissions: user.UserPermissions },
  });

  return res.status(200).json({
    accessToken: token,
  });
}
