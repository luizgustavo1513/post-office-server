import { Request, Response } from 'express';
import { Validator } from '../../../../../utils/validator/validator';
import { UserServices } from '../services/userServices';

const validator = new Validator();
const userServices = new UserServices();

export async function changePasswordController(req: Request, res: Response) {
  const { userId } = req.params;
  const { oldPassword, newPassword } = req.body;

  validator.notNull([
    { label: 'id de usuário', variable: userId },
    { label: 'senha antiga', variable: oldPassword },
    { label: 'senha nova', variable: newPassword },
  ]);

  validator.typeof([
    { label: 'id de usuário', type: 'string', variable: userId },
    { label: 'senha antiga', type: 'string', variable: oldPassword },
    { label: 'senha nova', type: 'string', variable: newPassword },
  ]);

  const user = await userServices.findById({ userId });

  validator.needExist([{ label: 'usuário', variable: user }]);

  await userServices.compareUserPassword({
    comparePassword: oldPassword,
    userPassword: user!.passwordHash,
  });

  await userServices.editPassword({ userId, password: newPassword });

  return res.sendStatus(200);
}
