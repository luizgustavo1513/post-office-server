import { Request, Response } from 'express';
import { Validator } from '../../../../../utils/validator/validator';
import { UserPermissionServices } from '../../userPermission/services/userPermissionServices';
import { UserServices } from '../services/userServices';

const validator = new Validator();
const user = new UserServices();
const userPermissionServices = new UserPermissionServices();

export async function createUserController(req: Request, res: Response) {
  const { name, email, password } = req.body;

  validator.notNull([
    { label: 'email', variable: email },
    { label: 'nome', variable: name },
    { label: 'senha', variable: password },
  ]);

  validator.typeof([
    { label: 'email', type: 'string', variable: email },
    { label: 'nome', type: 'string', variable: name },
    { label: 'senha', type: 'string', variable: password },
  ]);

  const existingEmail = await user.findByActiveEmail({ email });

  validator.cannotExists([{ label: 'email', variable: existingEmail }]);

  const createdUser = await user.create({
    email,
    name,
    passwordHash: password,
  });

  const postManPerm = await userPermissionServices.findByName({
    permName: 'PostMan',
  });

  validator.needExist([
    { label: 'permissão de carteiro', variable: postManPerm },
  ]);

  await userPermissionServices.createUserPermission({
    permissionId: postManPerm!.id,
    userId: createdUser.id,
  });

  return res.sendStatus(200);
}
