import { Request, Response } from 'express';
import { Validator } from '../../../../../utils/validator/validator';
import { UserServices } from '../services/userServices';

const user = new UserServices();
const validator = new Validator();

export async function updateUserController(req: Request, res: Response) {
  const { userId } = req.params;
  const { name, profilePicture } = req.body;

  validator.notNull([{ label: 'Id de usuário', variable: userId }]);

  if (profilePicture) {
    validator.typeof([
      { label: 'foto de perfil', type: 'string', variable: profilePicture },
    ]);
  }

  validator.typeof([{ label: 'nome', type: 'string', variable: name }]);

  const existingUser = await user.findById({ userId });

  validator.needExist([{ label: 'usuário', variable: existingUser }]);

  await user.edit({
    name,
    userId,
    profilePicture,
  });

  return res.sendStatus(200);
}
