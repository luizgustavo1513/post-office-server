// PRISMA
import { compare, hashSync } from 'bcrypt';
import { ServerMessage } from '../../../../../utils/messages/serverMessage';
import { prisma } from '../../../../../utils/prismaClient';

// TYPES
import { IEditUser, IEditUserPassword, IListUser } from '../types';

export class UserServices {
  async create({
    name,
    email,
    passwordHash,
  }: {
    name: string;
    email: string;
    passwordHash: string;
  }) {
    const user = prisma.user.create({
      data: {
        name,
        email: email.toLowerCase(),
        passwordHash: hashSync(passwordHash, 12),
      },
    });
    return user;
  }

  async findByActiveEmail({ email }: { email: string }) {
    return prisma.user.findFirst({
      select: {
        id: true,
        name: true,
        email: true,
        updatedAt: true,
        createdAt: true,
        UserPermissions: true,
      },

      where: { email: email.toLowerCase() },
    });
  }

  async updateLastAccess({ userId }: { userId: string }) {
    await prisma.user.update({
      data: {
        lastAccess: new Date(new Date().setHours(new Date().getHours() - 3)),
      },
      where: { id: userId },
    });
  }

  async edit({ userId, name, profilePicture }: IEditUser) {
    await prisma.user.update({
      data: {
        name,
        profilePicture,
      },
      where: { id: userId },
    });
  }

  async editPassword({ userId, password }: IEditUserPassword) {
    await prisma.user.updateMany({
      data: {
        passwordHash: hashSync(password, 12),
      },
      where: { id: userId },
    });
  }

  async delete({ userId }: { userId: string }) {
    await prisma.user.delete({ where: { id: userId } });
  }

  async findByEmailForEdit({
    email,
    userId,
  }: {
    email: string;
    userId: string;
  }) {
    return prisma.user.findFirst({
      select: {
        id: true,
        name: true,
        email: true,
        updatedAt: true,
        createdAt: true,
      },
      where: {
        email: email.toLowerCase(),
        NOT: {
          id: userId,
        },
      },
    });
  }

  async findById({ userId }: { userId: string }) {
    return prisma.user.findUnique({
      select: {
        id: true,
        name: true,
        email: true,
        profilePicture: true,
        passwordHash: true,
        updatedAt: true,
        createdAt: true,
      },
      where: { id: userId },
    });
  }

  async compareUserPassword({
    comparePassword,
    userPassword,
  }: {
    comparePassword: string;
    userPassword: string;
  }) {
    const validPassword = await compare(comparePassword, userPassword);

    if (!validPassword) {
      throw new ServerMessage({
        statusCode: 400,
        message: 'Senha atual incorreta',
      });
    }
  }

  async updateProfilePicture({
    profilePicture,
    userId,
  }: {
    userId: string;
    profilePicture: string;
  }) {
    await prisma.user.update({
      data: {
        profilePicture,
      },
      where: {
        id: userId,
      },
    });
  }

  async list({ loggedUserId, take = 20, page, search = '' }: IListUser) {
    const users = await prisma.user.findMany({
      select: {
        id: true,
        name: true,
        email: true,
        lastAccess: true,
        updatedAt: true,
        createdAt: true,
      },

      take,
      skip: (page - 1) * take,

      where: {
        name: {
          contains: search,
          mode: 'insensitive',
        },
        NOT: {
          id: loggedUserId,
        },
      },
      orderBy: {
        name: 'asc',
      },
    });

    const usersCount = await prisma.user.count({
      where: {
        name: {
          contains: search,
          mode: 'insensitive',
        },
        NOT: {
          id: loggedUserId,
        },
      },
    });

    return { users, usersCount };
  }
}
