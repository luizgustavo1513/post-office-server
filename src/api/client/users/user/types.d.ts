export interface IEditUser {
  userId: string;
  name?: string;
  profilePicture?: string;
  phoneNumber?: string;
}

export interface IEditUserPassword {
  userId: string;
  password: string;
}

export interface IListUser {
  loggedUserId: string;

  take?: number;
  page: number;
  search: string;
}
