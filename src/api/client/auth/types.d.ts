export interface IResponseFindByEmail {
  id: string;
  name: string;
  email: string;
  passwordHash: string;
  lastAccess?: Date;
  createdAt: Date;
  updatedAt: Date;
  UserPermissions: { Permission: { name: string } }[];
}
