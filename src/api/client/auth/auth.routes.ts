// LIBS
import { Router } from 'express';

// FUNCTIONS
import { authValidateToken } from './controllers/authValidateToken';

// MIDDLEWARES
import { authMiddleware } from '../../../middlewares/auth';

// ROUTES
export const authRouter = Router();

authRouter.get('/validate/token', authMiddleware, authValidateToken);
