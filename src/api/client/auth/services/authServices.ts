// LIBS
import { compare } from 'bcrypt';

// PRISMA
import { prisma } from '../../../../utils/prismaClient';

// CLASS
import { ServerMessage } from '../../../../utils/messages/serverMessage';
import { IResponseFindByEmail } from '../types';

export class AuthServices {
  async findByEmail({ email }: { email: string }) {
    const user = await prisma.user.findFirst({
      include: {
        UserPermissions: {
          select: {
            Permission: {
              select: {
                name: true,
              },
            },
          },
        },
      },
      where: {
        email: {
          equals: email,
          mode: 'insensitive',
        },
      },
    });
    return user as IResponseFindByEmail;
  }

  async validateToken({ userId }: { userId: string }) {
    const user = await prisma.user.findUnique({
      include: {
        UserPermissions: {
          select: { Permission: { select: { name: true } } },
        },
      },
      where: { id: userId },
    });
    return user;
  }

  async canLogin({
    user,
    password,
  }: {
    user: IResponseFindByEmail;
    password: string;
  }) {
    const isValuePassword = await compare(password, user.passwordHash);

    if (!isValuePassword) {
      throw new ServerMessage({
        statusCode: 400,
        message: 'E-mail ou senha incorretos.',
      });
    }
  }
}
