import { Router } from 'express';

import swaggerUi from 'swagger-ui-express';
import swaggerFile from './docs/swagger.json';

import { authMiddleware } from '../../middlewares/auth';
import { uploadRouter } from '../shared/upload/upload.routes';
import { permissionRouter } from './permission/permission.routes';
import { userRouter } from './users/user.routes';
import { authRouter } from './auth/auth.routes';
import { packageRouter } from './packages/package.routes';

export const clientRouter = Router();

clientRouter.use('/docs', swaggerUi.serve, swaggerUi.setup(swaggerFile));

clientRouter.use('/user', userRouter);
clientRouter.use('/auth', authRouter);
clientRouter.use('/permission', authMiddleware, permissionRouter);
clientRouter.use('/package', packageRouter);

clientRouter.use('/upload', authMiddleware, uploadRouter);
